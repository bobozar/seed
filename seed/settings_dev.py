from seed_base_settings import *

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SECRET_KEY = ')g#+t1qpv#%cdt_6dbe=gd-7cktle@q!0iuks=_nd=dmyu=+p='

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_PATH,'../media/')


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.

# Example: "/home/media/media.lawrence.com/static/"

STATICFILES_DIRS = (
     os.path.join(BASE_DIR, 'static'),
)

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')

STATIC_URL = '/static/'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
