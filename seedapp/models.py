from __future__ import unicode_literals
from django.forms import ModelForm
from django.db import models
from django import forms
import datetime

# Create your models here.

class Tester(models.Model):
    first_name = models.CharField(max_length=100, verbose_name="First Name")
    last_name = models.CharField(max_length=100, verbose_name="Last Name")
    email_address = models.EmailField()
    pub_date=models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.pub_date=datetime.datetime.now()
        return super(Tester, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.first_name


class TesterForm(forms.ModelForm):
    class Meta:
        model = Tester
        fields = ['first_name','last_name', 'email_address']
    
