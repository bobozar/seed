from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages
from django.shortcuts import redirect
from seedapp.models import Tester, TesterForm
# Create your views here.

def homepage(request):
    return render(request, 'main/home.html')

def add_tester(request):
    if request.method=="POST":
        form=TesterForm(request.POST)
        if form.is_valid():
            data=form.cleaned_data
            newdata=Tester(
                first_name = data['first_name'],
                last_name = data['last_name'],
                email_address=data['email_address'])
            newdata.save()
            messages.info(request, 'Data Added')
            return redirect('add_tester')
    else:
        form=TesterForm()
    return render(request, 'main/add.html',{'form':form})

def list_tester(request):
    try:
        all_tests = Tester.objects.all().order_by('-pub_date')
    except Tester.DoesNotExist:
        all_tests=None
    return render(request, 'main/list.html',{'all_tests':all_tests})




                  
